import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Agregar from '../components/Agregar'
import Modificar from '../components/Modificar'
import Eliminar from '../components/Eliminar'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/agregar',
      name: 'Agregar',
      component: Agregar
    },
    {
      path: '/modificar',
      name: 'Modificar',
      component: Modificar
    },
    {
      path: '/eliminar',
      name: 'Eliminar',
      component: Eliminar
    },
    
  ],
  mode : 'history'
})
