CREATE DATABASE operaciones;

CREATE TABLE desembolsos (
  id SERIAL NOT NULL PRIMARY KEY,
  nombre_ac varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  clientes int NOT NULL,
  monto numeric(8,2) NOT NULL,
  banco varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  fecha_desembolso date NOT NULL,
  sucursal varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  regional varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  
);

CREATE TABLE desembolsos (id SERIAL PRIMARY KEY, nombre_ac varchar(50) , clientes int , monto numeric(8,2) , banco varchar(50) , fecha_desembolso date , sucursal varchar(50) , regional varchar(50));

/**/
CREATE TABLE test_desembolso (id SERIAL PRIMARY KEY, nombre_ac varchar(50) , clientes int , monto numeric(8,2) , idBanco smallint , fecha_desembolso date , idSucursal smallint , idRegional smallint);
alter table test_desembolso add constraint FK_banco foreign key (idBanco) references banco(idBanco);
alter table test_desembolso add constraint FK_sucursal foreign key (idSucursal) references sucursal(idSucursal);
alter table test_desembolso add constraint FK_regional foreign key (idRegional) references regional(idRegional);

CREATE TABLE banco (idBanco SERIAL PRIMARY KEY, nombreBanco varchar(50));
CREATE TABLE sucursal (idSucursal SERIAL PRIMARY KEY, nombreSucursal varchar(50));
CREATE TABLE regional (idRegional SERIAL PRIMARY KEY, nombreRegional varchar(50));

INSERT INTO test_desembolso (nombre_ac, clientes, monto, idBanco, fecha_desembolso, idSucursal, idRegional) VALUES ('TEST', 10, 50000.00, 1, '2021-06-10', 1, 1), ('TEST_2', 10, 10000.00, 2, '2021-06-09', 2, 1);
INSERT INTO test_desembolso (nombre_ac, clientes, monto, idBanco, fecha_desembolso, idSucursal, idRegional) VALUES ('TEST_3', 10, 40000.00, 1, '2021-06-10', 1, 1), ('TEST_4', 10, 20000.00, 2, '2021-06-09', 2, 1);
INSERT INTO banco (idBanco, nombreBanco) VALUES (1, 'bancomer'), (2, 'banamex');
INSERT INTO sucursal (idSucursal, nombreSucursal) VALUES (1, 'xalapa'), (2, 'teziutlan'), (3,'oaxaca'), (4, 'puebla');
INSERT INTO regional (idRegional, nombreRegional) VALUES (1, 'veracruz'), (2, 'puebla'), (3,'oaxaca');

SELECT test_desembolso.id, test_desembolso.nombre_ac, test_desembolso.clientes, test_desembolso.monto, banco.nombreBanco, test_desembolso.fecha_desembolso, test_desembolso.idSucursal, test_desembolso.idRegional FROM test_desembolso INNER JOIN banco ON test_desembolso.idBanco = banco.idBanco WHERE test_desembolso.fecha_desembolso BETWEEN '2021-06-10' AND '2021-06-10' AND test_desembolso.idSucursal = 1;
SELECT test_desembolso.id, test_desembolso.nombre_ac, test_desembolso.clientes, test_desembolso.monto, banco.nombreBanco, test_desembolso.fecha_desembolso, sucursal.nombreSucursal, regional.nombreRegional FROM test_desembolso INNER JOIN banco ON test_desembolso.idBanco = banco.idBanco INNER JOIN sucursal ON test_desembolso.idSucursal = sucursal.idSucursal INNER JOIN regional ON test_desembolso.idRegional = regional.idRegional WHERE test_desembolso.fecha_desembolso BETWEEN '2021-06-10' AND '2021-06-10' AND sucursal.nombreSucursal = 'xalapa';


/**/

INSERT INTO desembolsos (id, nombre_ac, clientes, monto, banco, fecha_desembolso, sucursal, regional) VALUES
(1, 'PODEROSAS', 10, 50000.00, 'bancomer', '2020-06-19', 'xalapa', 'veracruz'),
(2, 'ESTRELLAS', 10, 10000.00, 'banamex', '2020-06-02', 'oaxaca', 'oaxaca'),
(3, 'LUNAS', 10, 10000.00, 'banamex', '2020-06-02', 'oaxaca', 'oaxaca'),
(4, 'SOLES', 10, 10000.00, 'banamex', '2020-06-02', 'oaxaca', 'oaxaca'),
(5, 'JARVIS', 10, 10000.00, 'banamex', '2020-06-02', 'oaxaca', 'oaxaca'),
(6, 'LUZ DE DIA', 10, 10000.00, 'banamex', '2020-06-02', 'puebla', 'veracruz'),
(7, 'LUZ DE LUNA', 10, 10000.00, 'banamex', '2020-06-02', 'puebla', 'puebla'),
(8, 'ARCOIRIS', 10, 10000.00, 'banamex', '2020-06-02', 'puebla', 'puebla'),
(9, 'TRABAJADORAS', 10, 10000.00, 'banamex', '2020-06-02', 'puebla', 'puebla'),
(10, 'EMPRENDEDORAS', 10, 10000.00, 'banamex', '2020-06-02', 'puebla', 'puebla'),
(12, 'SUPERPODEROSAS', 10, 10000.00, 'banamex', '2020-06-02', 'teziutlan', 'veracruz'),
(13, 'LAS CONSENTIDAS', 10, 10000.00, 'banamex', '2020-06-02', 'teziutlan', 'veracruz'),
(14, 'LAS AGUILAS', 10, 10000.00, 'banamex', '2020-06-02', 'teziutlan', 'veracruz'),
(15, 'LAS BELLAS', 10, 10000.00, 'banamex', '2020-06-02', 'teziutlan', 'veracruz'),
(16, 'TEZIUTLAN', 10, 10000.00, 'banamex', '2020-06-02', 'teziutlan', 'veracruz'),
(17, 'XALAPA', 10, 10000.00, 'banamex', '2020-06-02', 'xalapa', 'veracruz'),
(18, 'PUEBLA', 10, 10000.00, 'banamex', '2020-06-02', 'xalapa', 'veracruz'),
(19, 'JAROCHITAS', 10, 10000.00, 'banamex', '2020-06-02', 'xalapa', 'veracruz'),
(20, 'SAN MIGUEL', 10, 10000.00, 'banamex', '2020-06-02', 'xalapa', 'veracruz');


/*Tabla de usuarios que serán logueados en el sistema*/
CREATE TABLE users (iduser SERIAL NOT NULL PRIMARY KEY, username varchar(50) NOT NULL, userpassword varchar(50) NOT NULL);

CREATE TABLE users (
  iduser SERIAL NOT NULL PRIMARY KEY, 
  username varchar(50) COLLATE utf8_spanish_ci NOT NULL, 
  userpassword varchar(50) COLLATE utf8_spanish_ci NOT NULL,
);

/*Insertar valores en tabla usuarios*/
INSERT INTO users (username, userpassword) VALUES
('gerente_sucursal', 'sucursal'),
('gerente_regional', 'regional');

/* Modificar columna monto en postgresql de tipo numerico a tipo money
  alter table desembolsos alter column monto type money;
  alter table desembolsos alter column monto type numeric(8,2);
*/

/*ALTER TABLE test_desembolso ALTER COLUMN monto TYPE int;*/