//equivalente a conection/conection.js

const { Pool } = require('pg');

const pool = new Pool({
    host: 'localhost', 
    user: 'postgres',
    password: 'admin',
    database: 'operaciones',
    port: '5432'

    /*connectionString: "postgres://tdiglvyugoikdi:1f67a8fd45d2bf852fd5ca877671b9d9b7c06ab4a5f29520389dfec37156f6de@ec2-35-174-122-153.compute-1.amazonaws.com:5432/dbj0fgep46pt6h",
    ssl: {
        rejectUnauthorized: false
    }*/
});

const get = async (req, res) => {
    res.send('home');
}

const getOperaciones = async (req, res) => {
    const response = await pool.query(`SELECT test_desembolso.id, test_desembolso.nombre_ac, test_desembolso.clientes, 
        test_desembolso.monto, banco.nombreBanco, to_char(test_desembolso.fecha_desembolso, 'YYYY-MM-DD') as fecha_desembolso, sucursal.nombreSucursal, 
        regional.nombreRegional FROM test_desembolso INNER JOIN banco ON test_desembolso.idBanco = banco.idBanco 
        INNER JOIN sucursal ON test_desembolso.idSucursal = sucursal.idSucursal INNER JOIN regional ON 
        test_desembolso.idRegional = regional.idRegional ORDER BY test_desembolso.id ASC`);
    if (response.rowCount > 0) {
        res.status(200).json(response.rows);
    } else {
        res.status(404).json(response.rows);
    }
}

const getDesembolsoById = async (req, res) => {
    //res.send('Desembolso ID: ' + req.params.id);
    const id = req.params.id;
    //const response = await pool.query('SELECT * FROM desembolsos WHERE id = $1', [id]);
    const response = await pool.query(`SELECT test_desembolso.id, test_desembolso.nombre_ac, test_desembolso.clientes, 
    test_desembolso.monto, banco.nombreBanco, to_char(test_desembolso.fecha_desembolso, 'YYYY-MM-DD') as fecha_desembolso, sucursal.nombreSucursal, 
    regional.nombreRegional FROM test_desembolso INNER JOIN banco ON test_desembolso.idBanco = banco.idBanco 
    INNER JOIN sucursal ON test_desembolso.idSucursal = sucursal.idSucursal INNER JOIN regional ON 
    test_desembolso.idRegional = regional.idRegional WHERE id = $1`, [id]);

    if (response.rowCount > 0) {
        res.status(200).json({
            status: 200,
            message: 'Done',
            credit: response.rows
        }
)
    } else {
        res.status(404).json({
            status: 404,
            message: 'Not Found',
            credit: response.rows
        })
    }
};

//Método de prueba para realizar consultas por un rango de fechas (2 parámetros) POR SUCURSAL
const getDesesmbolsosByDateSucursal = async (req,res) => {
    const { fecha_1, fecha_2, sucursal } = req.body;
    //const response = await pool.query('SELECT * FROM desembolsos WHERE fecha_desembolso BETWEEN $1 AND $2 AND sucursal = $3', [fechaSuc_1, fechaSuc_2, sucursalInput]);
    const response = await pool.query(`SELECT test_desembolso.id, test_desembolso.nombre_ac, test_desembolso.clientes, 
        test_desembolso.monto, banco.nombreBanco, to_char(test_desembolso.fecha_desembolso, 'YYYY-MM-DD') as fecha_desembolso, sucursal.nombreSucursal, 
        regional.nombreRegional FROM test_desembolso INNER JOIN banco ON test_desembolso.idBanco = banco.idBanco 
        INNER JOIN sucursal ON test_desembolso.idSucursal = sucursal.idSucursal INNER JOIN regional ON 
        test_desembolso.idRegional = regional.idRegional WHERE test_desembolso.fecha_desembolso BETWEEN $1 AND $2 
        AND sucursal.nombreSucursal = $3 ORDER BY test_desembolso.fecha_desembolso ASC`, [fecha_1, fecha_2, sucursal]);
    if (response.rowCount > 0) {
        res.status(200).json({
            status: 200,
            message: 'Done',
            credit: response.rows
        })
    } else {
        res.status(404).json({
            status: 404,
            message: 'Not Found',
            credit: response.rows
        })
    }
    //console.log(response);
}

//Método de prueba para realizar consultas por un rango de fechas (2 parámetros) POR REGIONAL
const getDesesmbolsosByDateRegional = async (req, res) => {
    const { fecha_1, fecha_2, regional } = req.body;
    const response = await pool.query(`SELECT test_desembolso.id, test_desembolso.nombre_ac, test_desembolso.clientes, 
    test_desembolso.monto, banco.nombreBanco, to_char(test_desembolso.fecha_desembolso, 'YYYY-MM-DD') as fecha_desembolso, sucursal.nombreSucursal, 
    regional.nombreRegional FROM test_desembolso INNER JOIN banco ON test_desembolso.idBanco = banco.idBanco 
    INNER JOIN sucursal ON test_desembolso.idSucursal = sucursal.idSucursal INNER JOIN regional ON 
    test_desembolso.idRegional = regional.idRegional WHERE test_desembolso.fecha_desembolso BETWEEN $1 AND $2 
    AND regional.nombreregional = $3 ORDER BY test_desembolso.fecha_desembolso ASC`, [fecha_1, fecha_2, regional]);
    if (response.rowCount > 0) {
        res.status(200).json({
            status: 200,
            message: 'Done',
            credit: response.rows
        })
    } else {
        res.status(404).json({
            status: 404,
            message: 'Not Found',
            credit: response.rows
        })
    }
    //console.log(response);
};

const createDesembolso = async (req, res) => { 
    const { nombre_ac, clientes, monto, idbanco, fecha_desembolso, idsucursal, idregional } = req.body;
    const response = await pool.query(`INSERT INTO test_desembolso (nombre_ac, clientes, monto, idBanco, fecha_desembolso, 
        idSucursal, idRegional) VALUES ($1, $2, $3, $4, $5, $6, $7)`, 
        [nombre_ac, clientes, monto, idbanco, fecha_desembolso, idsucursal, idregional]);
    /*res.status(201).json({
        mensaje: 'Desembolso añadido satisfactoriamente.',
        body: {
            desembolso: {nombre_ac, clientes, monto, idbanco, fecha_desembolso, idsucursal, idregional}
        }
    })*/
    if (response.rowCount > 0) {
        res.status(201).json({
            status: 201,
            message: `Desembolso ${nombre_ac} creado exitosamente.`,
            credit: {nombre_ac, clientes, monto, idbanco, fecha_desembolso, idsucursal, idregional}
        })
    } else {
        res.status(400).json({
            status: 400,
            message: 'Bad Request',
            credit: response.rows
        })
    }
};

const updateDesmbolso = async (req, res) => {
    const id = req.params.id;
    const { nombre_ac, clientes, monto, idbanco, fecha_desembolso, idsucursal, idregional } = req.body;
    const response = await pool.query('UPDATE test_desembolso SET nombre_ac = $1, clientes = $2, monto = $3, idbanco = $4, fecha_desembolso = $5, idsucursal = $6, idregional = $7 WHERE id = $8', [
        nombre_ac, 
        clientes, 
        monto, 
        idbanco, 
        fecha_desembolso, 
        idsucursal, 
        idregional,
        id
    ])
    if (response.rowCount > 0) {
        res.status(201).json({
            status: 201,
            message: `Crédito actualizado satisfactoriamente.`,
            credit: {nombre_ac, clientes, monto, idbanco, fecha_desembolso, idsucursal, idregional}
        })
    } else {
        res.status(400).json({
            status: 400,
            message: 'Bad Request',
            credit: response.rows
        })
    }
};

const deleteDesembolso = async (req, res) => {
    const id = req.params.id;
    const response = await pool.query('DELETE FROM test_desembolso WHERE id = $1', [id]);
    if (response.rowCount > 0) {
        res.status(200).json({
            status: 200,
            message: `Desembolso con ID ${id} eliminado satisfactoriamente.`,
            credit: response.rows.nombre_ac
        })
    } else {
        res.status(400).json({
            status: 400,
            message: 'Not Found',
            credit: response.rows
        })
    }
};

module.exports = {
    get,
    getOperaciones,
    getDesembolsoById,
    createDesembolso,
    updateDesmbolso,
    deleteDesembolso,

    getDesesmbolsosByDateSucursal,
    getDesesmbolsosByDateRegional,
}