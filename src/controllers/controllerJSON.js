const jwt = require('jsonwebtoken');
const { Pool } = require('pg');
const router = require('../routes');

const pool = new Pool({
    /*host: 'localhost', 
    user: 'postgres',
    password: 'admin',
    database: 'operaciones',
    port: '5432'*/
    connectionString: "postgres://tdiglvyugoikdi:1f67a8fd45d2bf852fd5ca877671b9d9b7c06ab4a5f29520389dfec37156f6de@ec2-35-174-122-153.compute-1.amazonaws.com:5432/dbj0fgep46pt6h",
    ssl: {
        rejectUnauthorized: false
    }
});

/*const getUsers = async (req, res) => {
    const response = await pool.query('SELECT * FROM users', (err, rows, fields) => {
        if (!err) {
            //res.status(200).json(response.rows);
            res.json(rows);
        } else {
            console.log(err);
        }        
    })
};*/

const getUsers = async (req, res) => {
    try {
        const response = await pool.query('SELECT * FROM users');
        if (response){
            res.status(200).json(response.rows);
        }
        //console.log(req.data);
    } catch (e) {
        console.log(e);
    }
}

const getLogin = async(req, res) => {
    //console.log(req.body);
    const { username, userpassword } = req.body;
    const response = await pool.query('SELECT * FROM users WHERE username= $1 AND userpassword= $2', 
    [username, userpassword]
    );
    if (response.rows.length > 0) {
        const token = jwt.sign(response.rows[0], 'secret');
        res.status(200).json({
            res: response.rows, 
            token: token
        });
        //console.log(token);
    } else {
        res.status(404).json({
            message: 'Datos incorrectos',
            body: { username, userpassword },
        })
    }
}

function verifyToken(req, res, next) {
    if (!req.headers.authorization) return res.status(401).json('Unauthorized');
    const token = req.headers.authorization.substr(7);
    //console.log(token);
    if (token !=='') {
        const content =  jwt.verify(token, 'secret');
        //console.log(content);
        req.data = content;
        next();
    } else {
        res.status(401).json('Token empty');
    }
}

module.exports = {
    getUsers,
    getLogin,
    verifyToken
}




