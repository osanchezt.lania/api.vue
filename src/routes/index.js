const { Router } = require('express');
const router = Router();

const { get, getOperaciones, createDesembolso, getDesembolsoById, deleteDesembolso, updateDesmbolso, getDesesmbolsosByDateSucursal, getDesesmbolsosByDateRegional } = require('../controllers/index.controller')
const { getUsers, getLogin, verifyToken } = require('../controllers/controllerJSON')

router.get('/', get);
router.get('/operaciones', getOperaciones);
router.get('/operaciones/:id', getDesembolsoById);
router.put('/operaciones/:id', updateDesmbolso);
router.post('/operaciones', createDesembolso);
router.delete('/operaciones/:id', deleteDesembolso);

router.post('/operaciones/sucursal', getDesesmbolsosByDateSucursal);
router.post('/operaciones/regional', getDesesmbolsosByDateRegional);

//rutas users
router.get('/users', verifyToken, getUsers);
router.post('/operaciones/singin', getLogin);

module.exports = router;